<br>

<div  style="color:White; background-color:brown">
  <div class="row">
    <div class="col-md-3 text-center">
      <h3>Ubicación</h3> <hr>
      <p>Calle: Av. Simón Rodríguez s/n Barrio El Ejido</p>
      <p>San felipe</p>
    </div>

    <div class="col-md-3 text-center">
      <h3>Redes Sociales</h3> <hr>
      <p>Siguenos en Facebook</p>
      <p>Siguenos en Youtube</p>
      <p>Siguenos en Instagram</p>
    </div>

    <div class="col-md-3 text-center">
      <h3>Contactos</h3> <hr>
      <p>Teléfono: (593) 03 2252205</p>
      <p>Celular: 225 2307</p>
    </div>

    <div class="col-md-3 text-center">
      <h3>Correo electrónico</h3> <hr>
      <p> comunicacion.institucional@utc.edu.ec</p>
      <p>Derechos UTC&copy </p>
    </div>

  </div>

</div>
</body>
</html>
