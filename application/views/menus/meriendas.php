<body style="color:White; background-color:black">
  <div class="container text-center">
    <h1>Listado de Meriendas</h1> <br>

    <div class="row">
      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/te.png" height="195" width="220">
          <div class="caption">
            <h3>Té</h3>
            <p>Precio $1,50</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/pancake.png" height="195" width="220">
          <div class="caption">
            <h3>Pancakes</h3>
            <p>Precio $2,00</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/capuchino.png" height="195" width="220">
          <div class="caption">
            <h3>Capuchino</h3>
            <p>Precio $1,75</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/tortilla.png" height="195" width="220">
          <div class="caption">
            <h3>Tortilla de Patata</h3>
            <p>Precio $2,00</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>
