<body style="color:White; background-color:black">
  <div class="container text-center">
    <h1>Listado de Desayunos</h1> <br>

    <div class="row">
      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/cafe.png" height="195" width="220">
          <div class="caption">
            <h3>Café expresso con tostadas</h3>
            <p>Precio $2,50</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/choco.jpg" height="195" width="220">
          <div class="caption">
            <h3>Chocolate caliente con pan integral</h3>
            <p>Precio $3,00</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/tocino.png" height="195" width="220">
          <div class="caption">
            <h3>Huevos con Tocino</h3>
            <p>Precio $2,75</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/estrellados.png" height="195" width="220">
          <div class="caption">
            <h3>Huevos estrellados</h3>
            <p>Precio $2,50</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>
