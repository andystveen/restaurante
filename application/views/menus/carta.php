<body style="color:White; background-color:black">
<h1  class="text-center">MENÚ PLATOS A LA CARTA</h1> <br>
<div class="container">
  <div class="row">
    <div class="col-md-3 text-center">
    <h3>Café expresso con tostadas.......... $2,50</h3> <br>
    <img src="<?php echo base_url(); ?>/assets/images/cafe.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Chocolate caliente con pan integral.......... $3,00</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/choco.jpg" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Huevos con tocino.......... $2,75</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/tocino.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Huevos estrellados.......... $2,50</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/estrellados.png" height="195" width="220">
    </div>
  </div>
</div>
<br>
<div class="container">
  <div class="row">
    <div class="col-md-3 text-center">
    <h3>Sopa de pollo.......... $2,00</h3> <br>
    <img src="<?php echo base_url(); ?>/assets/images/consome.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Sancocho.......... $2,00</h3> <br> <br>
      <img src="<?php echo base_url(); ?>/assets/images/sancocho.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Seco de pollo.......... $2,75</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/secop.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Arroz con pollo frito.......... $2,50</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/pollo.png" height="195" width="220">
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-3 text-center">
    <h3>Arroz con carne y menestra.......... $2,75</h3> <br>
    <img src="<?php echo base_url(); ?>/assets/images/menestra.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Arroz con camarón.......... $3,50</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/camaron.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Ensalada.......... $2,25</h3> <br> <br>
      <img src="<?php echo base_url(); ?>/assets/images/ensalada.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Sopa de verduras.......... $2,50</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/verduras.png" height="195" width="220">
    </div>
  </div>
</div>
<br>
<div class="container">
  <div class="row">
    <div class="col-md-3 text-center">
    <h3>Té.......... $1,50</h3> <br>
    <img src="<?php echo base_url(); ?>/assets/images/te.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Pancakes.......... $2,00</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/pancake.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Capuchino.......... $1,75</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/capuchino.png" height="195" width="220">
    </div>

    <div class="col-md-3 text-center">
      <h3>Tortilla de patata.......... $2,00</h3> <br>
      <img src="<?php echo base_url(); ?>/assets/images/tortilla.png" height="195" width="220">
    </div>
  </div>
</div>
</body>
