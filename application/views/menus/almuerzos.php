<body style="color:White; background-color:black">
  <div class="container text-center">
    <h1>Listado de Almuerzos</h1> <br>

    <div class="row">
      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/consome.png" height="195" width="220">
          <div class="caption">
            <h3>Sopa de Pollo</h3>
            <p>Precio $2,00</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/sancocho.png" height="195" width="220">
          <div class="caption">
            <h3>Sancocho</h3>
            <p>Precio $2,00</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/secop.png" height="195" width="220">
          <div class="caption">
            <h3>Seco de Pollo</h3>
            <p>Precio $2,75</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="thumbnail">
          <img src="<?php echo base_url(); ?>/assets/images/pollo.png" height="195" width="220">
          <div class="caption">
            <h3>Arroz con Pollo frito</h3>
            <p>Precio $2,50</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
          </div>
        </div>
      </div>
    </div>
    <br>
    <div class="col-md-3">
      <div class="thumbnail">
        <img src="<?php echo base_url(); ?>/assets/images/menestra.png" height="195" width="220">
        <div class="caption">
          <h3>Arroz con Carne y menestra</h3>
          <p>Precio $2,75</p>
          <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="thumbnail">
        <img src="<?php echo base_url(); ?>/assets/images/camaron.png" height="195" width="220">
        <div class="caption">
          <h3>Arroz con Camarón</h3>
          <p>Precio $3,50</p>
          <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="thumbnail">
        <img src="<?php echo base_url(); ?>/assets/images/ensalada.png" height="195" width="220">
        <div class="caption">
          <h3>Ensalada</h3>
          <p>Precio $2,25</p>
          <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="thumbnail">
        <img src="<?php echo base_url(); ?>/assets/images/verduras.png" height="195" width="220">
        <div class="caption">
          <h3>Sopa de Verduras</h3>
          <p>Precio $2,50</p>
          <p><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
        </div>
      </div>
    </div>
  </div>
  </div>

</body>
